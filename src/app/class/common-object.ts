import { JwtHelperService } from "@auth0/angular-jwt";

export const jwtHelper = new JwtHelperService()
export const dbKeys = {
    admin: 'admins',
    blocked_user: 'blocked_user',
    chat_room: 'chat_room',
    chatroom_info: 'chatroom_info',
    fcmKeys: 'fcmKeys',
    reported_message: 'reported_message',
    user_info: 'user_info',
    blocked_in_chatroom: 'blocked_in_chatroom',
    map_user_info: 'map_user_info'

}
export const storageKeys = {
  group_icons: 'group_icons'
}
export function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }