export interface UserInfoAdminMap{
    displayName: string,
    email: string,
    phoneNumber: string,
    uid: string,
    disabled: boolean
}