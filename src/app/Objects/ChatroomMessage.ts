import { firestore } from 'firebase'

export interface ChatroomMessage{
    key: string,
    type: ChatroomMessageType,
    blocked: boolean,
    sender: string,
    created_at: Date,
    chatroom_id: string,
    message: string,
}

export function ChatroomMessageFromAnyObject(anyObj: any):ChatroomMessage{
    const newObj = <ChatroomMessage> anyObj
    if(anyObj.created_at){
    newObj.created_at = (<firestore.Timestamp> anyObj.created_at).toDate()
    }else{
        newObj.created_at = new Date()
    }
    return newObj
    
}
export enum ChatroomMessageType{
    text='text',
    image='image',
    blocked='blocked'
}