import { LocateCountryCode, LocateLanguageCode } from './LocateInfo';
export interface ChatroomInfo {
    key: string,
    title: string,
    description: string,
    rules: string,
    locate: {
        country: LocateCountryCode,
        language: LocateLanguageCode 
    },
    image: string,
}