export interface ReportedMessage{
    chatroomId: string,
    key: string,
    messageId: string,
    reportReason: string,
    reporterId: string,
    status: ReportedMessageStatus
}
export enum ReportedMessageStatus{
    pending='pending',
    blocked='blocked',
    ignored='ignored'
}