
import { firestore } from 'firebase';
export interface UserInfo{
    user_id: string,
    jobined_chatroom: Array<string>,
    display_name: string,
    blocked: boolean,
    createdAt : firestore.Timestamp
}