export interface TopLevelNavItem{
    displayName: String,
    children: Array<SubLevelNavItem>
}
export interface SubLevelNavItem{
    path: String,
    displayName: String
}