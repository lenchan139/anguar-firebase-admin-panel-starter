import { AngularFireAuth } from '@angular/fire/auth';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { TopLevelNavItem } from '../Objects/NavigationItems';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})
export class NavigationComponent {
  isForceHideSideBar = false
  currentUser : firebase.User = null
  navItems: Array<TopLevelNavItem> = [{
    displayName: '用戶設定',
    children: [
      {
        displayName: '用戶一覽',
        path: '/panel/users'
      }
    ]
  },
  {
    displayName: '聊天室設定',
    children: [
      {
        displayName: '聊天室資料',
        path: '/panel/chatroomSettings'
      },
      {
        displayName: '查看聊天室',
        path: '/panel/chatroomMessages'
      },
      {
        displayName: '已舉報信息',
        path: '/panel/reportedMessages'
      }
    ]
  },

  {
    displayName: '01',
    children: [
      {
        displayName: '02',
        path: '/panel/chatroomSettings'
      }
    ]
  },
  ]
  get displayForceHideSideBar() {
    if (this.isForceHideSideBar) {
      return 'none'
    } else {
      return ''
    }
  }
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  initMarginLeft = ''
  constructor(
    private breakpointObserver: BreakpointObserver,
    public afAuth: AngularFireAuth,
    public router :Router

  ) {

    afAuth.authState.subscribe(user => {
      this.currentUser = user
      const s = <HTMLDivElement>document.getElementById('content')
      this.initMarginLeft = s.style.marginLeft
      if (user && user.uid) {
        this.isForceHideSideBar = false
        s.style.marginLeft = this.initMarginLeft
      } else {
        this.isForceHideSideBar = true
        s.style.marginLeft = ''
      }

    })
  }
  async signout(){
    await this.afAuth.signOut()
    location.href = '/'
    //this.router.navigate([''])
  }
}
