import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-mainpage',
  templateUrl: './panel-mainpage.component.html',
  styleUrls: ['./panel-mainpage.component.less']
})
export class PanelMainpageComponent implements OnInit {

  constructor(
    public afAuth : AngularFireAuth,
    public router : Router
  ) { }

  ngOnInit(): void {
    this.afAuth.authState.subscribe(u=>{
      if(!u || !u.uid){
        this.router.navigate(['login'])
      }
    })
  }

}
