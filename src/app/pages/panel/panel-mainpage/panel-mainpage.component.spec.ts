import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelMainpageComponent } from './panel-mainpage.component';

describe('PanelMainpageComponent', () => {
  let component: PanelMainpageComponent;
  let fixture: ComponentFixture<PanelMainpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelMainpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelMainpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
