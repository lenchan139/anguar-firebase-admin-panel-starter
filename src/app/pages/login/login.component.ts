import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { jwtHelper } from '../../class/common-object';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm = {
    email: '',
    password: ''
  }
  formId = `masdoanf0_as_jdaiojgi`
  constructor(
    public afAuth: AngularFireAuth,
    public router : Router
  ) { }

  ngOnInit(): void {
  }
  async onSubmit(){
    const form = <HTMLFormElement> document.getElementById(this.formId)
    if(form && form.reportValidity()){
      try{
        const authInfo = await this.afAuth.signInWithEmailAndPassword(this.loginForm.email, this.loginForm.password)
        const dToken = await authInfo.user.getIdToken()
        const token = jwtHelper.decodeToken(dToken)
        const isAdmin = token.isAdmin
        if(isAdmin){
          //this.router.navigate(['panel'])
          location.href = '/panel'
        }else{
          alert('你不是管理員。\nYou are not admin.')
          await this.afAuth.signOut()
        }
      }catch(e){
        console.error(e)
        alert(`ERROR 錯誤！\nCODE: ${e.code}`)
        const code = e.code
       switch(code){
         case 'auth/user-not-found':
          alert('沒有此用戶！')
         default:
           alert(`ERROR 錯誤！\nCODE: ${e.code}`)
       }
      }
    }
  }
}
